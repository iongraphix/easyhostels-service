Easy Hostels REST API
====================
This is a **Laravel** based REST API for the EasyHostels Ecosystem.

> Written with [StackEdit](https://stackedit.io/).
> Created By **Peace Okenwa**