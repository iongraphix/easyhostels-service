<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\HostelRating;

class Hostel extends Model
{
    protected $fillable = [
        'display_name', 'address', 'short_description', 'contact_phone', 'lat', 'lng', 'display_price', 'user_id', 'available', 'notes'
    ];

    public function  getPrimary(){
        $qm = ["hostel_id" => $this->id, "type" => "Primary"];
        return HostelImagery::where($qm)->first();
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function ratings(){
        return $this->hasMany('App\HostelRating');
    }
    public function images()
    {
        return $this->hasMany('App\HostelImagery');
    }
    public function rooms()
    {
        return $this->hasMany('App\HostelRoom');
    }
    public function facilities(){
    	return $this->belongsToMany('App\FacilityType', 'hostel_facilities', 'hostel_id', 'ftype_id');
    }
    public function policies(){
    	return $this->hasMany('App\HostelPolicy');	
    }
    public function users_who_rated(){
    	return $this->belongsToMany('App\User', 'hostel_ratings', 'hostel_id', 'user_id');	
    }
}
