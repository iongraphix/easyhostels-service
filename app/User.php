<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function owned_hostels()
    {
        return $this->hasMany('App\Hostel');
    }

    public function rated_hostels(){
        return $this->belongsToMany('App\Hostel', 'hostel_ratings', 'user_id', 'hostel_id');
    }

    public function booked_rooms(){
        return $this->belongsToMany('App\HostelRoom', 'hostel_room_bookings', 'user_id', 'hostel_room_id');
    }
}
