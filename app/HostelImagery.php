<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HostelImagery extends Model
{
    //
    protected $fillable = [
        'hostel_id', 'type', 'image_path'
    ];
    
    public function hostel()
    {
        return $this->belongsTo('App\Hostel');
    }
}
