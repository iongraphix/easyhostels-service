<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Hostel;
use App\HostelImagery;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class HostelController extends Controller
{
    // Rest Functions

    public function index()
    {
        $hostels = Hostel::all();
        foreach ($hostels as $hostel) {
            $hostel["rating"] = $hostel->ratings()->avg("rating");
            if($hostel["rating"] == null){
                $hostel["rating"] = 0.0;
            }
        }
        return response()->json($hostels);
    }

    public function filter($query){

       $results = DB::select( DB::raw("SELECT * FROM `hostels`"));

      return response()->json($results);

    }
    public function base64_to_jpeg($base64_string, $output_file) {
        $ifp = fopen($output_file, "wb"); 

        $data = explode(',', $base64_string);
        if(isset($data[1])){
        fwrite($ifp, base64_decode($data[1])); 
        }
        fclose($ifp); 

        return $output_file; 
    }
    
    public function addPrimary(Request $request){
        $ladata = Input::all();
        $hid = $ladata["hostel_id"];

        $hostel = Hostel::find($hid);
        $hostel->thumbnail = ".jpg";
        if(isset($ladata["image"])){
            $this->base64_to_jpeg($ladata["image"], 'images/hostel/'.$hid.".jpg");
        }
        $nu = $hostel->save();
        return response()->json(array("status" => "success", "info" => $nu));
    }
    public function uploadPrimary(){
        $ladata = $request->only('hostel_id', 'image');
        $hostel  = Hostel::find($ladata["id"]);
        $hostel->thumbnail = $ladata["image"];
        $nu = $hostel->save();
        return response()->json(array("status" => "success", "info" => $nu));
    }

    public function owning($id){
        $hostels = Hostel::where('user_id', '=', $id)->get();
        return response()->json($hostels);
    }

    public function store(Request $request)
    {   
        $data = Input::all();
        $hostel = Hostel::create($data);
        return response()->json(array("status" => "success", "info" => $hostel));
    }
    
    public function show($id)
    {

        $hostel = Hostel::find($id);
        $hostel["rating"] = $hostel->ratings()->avg("rating");
        if($hostel["rating"] == null){
                $hostel["rating"] = 0.0;
        }
        return response()->json($hostel);
    }

    public function update(Request $request, $id)
    {   
        $hostel  = Hostel::find($id);
        $hostel->display_name = Input::get('display_name');
        $hostel->address = Input::get('address');
        $hostel->short_description = Input::get('short_description');
        $hostel->contact_phone = Input::get('contact_phone');
        $hostel->contact_email = Input::get('contact_email');
        $hostel->lat = Input::get('lat');
        $hostel->lng = Input::get('lng');
        $hostel->display_price = Input::get('display_price');
        
        $nu = $hostel->save();
        return response()->json(array("status" => "success", "info" => $nu));
    }
    
    public function destroy($id)
    {
        $hostel = Hostel::find($id);
        $hostel->delete();
        return response()->json(array("status" => "success", "action" => "destroy"));
    }

    // Show Functions

    public function showPhotos($id){
        $hostel = Hostel::find($id);
        $images = $hostel->images;

        return response()->json($images);
    }

    public function showRooms($id){
        $hostel = Hostel::find($id);
        $rooms = $hostel->rooms;
        return response()->json($rooms);
    }

    public function showPolicies($id){
        $hostel = Hostel::find($id);
        $policies = $hostel->policies;
        return response()->json($policies);
    }

    public function showFacilities($id){
        $hostel = Hostel::find($id);
        $facilities = $hostel->facilities;
        return response()->json($facilities);
    }

    public function showReviews($id){
        $hostel = Hostel::find($id);
        $users = $hostel->users_who_rated;
        //dd($users);
        return response()->json($users);
    }

   
}
