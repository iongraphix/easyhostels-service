<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;

class LocatorController extends Controller
{
    //
    public function index(){

    }
    public function radiusBased($lat,$lng,$rad){
    	$data = array("lat" => $lat, "lng" => $lng, "radius" => $rad);
        
        $results = DB::select( DB::raw("SELECT *, ( 3959 * acos( cos( radians(:lat1) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(:lng) ) + sin( radians(:lat2) ) * sin( radians( lat ) ) ) ) AS distance FROM hostels HAVING distance < :radius ORDER BY distance LIMIT 0 , 20"), array(
           'lat1' => $lat,
           'lat2' => $lat,
           'lng' => $lng,
           'radius' => $rad
         ));

 	  return response()->json($results);
    }

    public function boundsBased($swlat,$swlng,$nwlat,$nwlng){
        $bigQuery = "SELECT * FROM hostels WHERE lat > :a AND lat < :c AND lng > :b AND lng < :d";
         $results = DB::select( DB::raw($bigQuery), array(
           'a' => $swlat,
           'b' => $swlng,
           'c' => $nwlat,
           'd' => $nwlng
         ));

    	return response()->json($results);
    }

    public function boundsBasedFilter($min,$max,$swlat,$swlng,$nwlat,$nwlng){
        $bigQuery = "SELECT * FROM hostels WHERE lat > :a AND lat < :c AND lng > :b AND lng < :d AND display_price BETWEEN :min AND :max;";
         $results = DB::select( DB::raw($bigQuery), array(
           'min' => $min,
           'max' => $max,
           'a' => $swlat,
           'b' => $swlng,
           'c' => $nwlat,
           'd' => $nwlng
         ));

        return response()->json($results);
    }

    public function radiusBasedFilter($min,$max,$lat,$lng,$rad){
        $data = array("lat" => $lat, "lng" => $lng, "radius" => $rad);
        
        $results = DB::select( DB::raw("SELECT *, ( 3959 * acos( cos( radians(:lat1) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(:lng) ) + sin( radians(:lat2) ) * sin( radians( lat ) ) ) ) AS distance FROM hostels WHERE display_price BETWEEN :min AND :max HAVING distance < :radius ORDER BY distance LIMIT 0 , 40"), array(
           'min' => $min,
           'max' => $max,
           'lat1' => $lat,
           'lat2' => $lat,
           'lng' => $lng,
           'radius' => $rad
         ));

      return response()->json($results);
    }
}
