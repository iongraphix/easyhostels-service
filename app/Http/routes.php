<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/auth', 'LoginController@normal_login');
Route::post('/upload/user', 'UploadController@user');
Route::get('/logout', 'LoginController@normal_logout');

Route::group(['prefix' => '/resource'], function () {
    
    // Role related urls
    Route::resource('role', 'RolesController');

    // User related urls
    Route::resource('user', 'UserController');
    
    // Hostel related urls
    Route::resource('hostel', 'HostelController');
    Route::post('/hostel/upload/owner', 'HostelController@addPrimary');
    
    Route::get('/hostel/owner/{id}', 'HostelController@owning');
    Route::get('/hostel/{id}/photos', 'HostelController@showPhotos');
    Route::get('/hostel/{id}/reviews', 'HostelController@showReviews');
    Route::get('/hostel/{id}/policies', 'HostelController@showPolicies');
    Route::get('/hostel/{id}/facilities', 'HostelController@showFacilities');
    Route::get('/hostel/{id}/rooms', 'HostelController@showRooms');
    Route::get('/hostel/filter/{query}', 'HostelController@filter');

    // Finding and Locating Hostels
    Route::get('/find/radius/{lat}/{lng}/{rad}', 'LocatorController@radiusBased');
    Route::get('/find/bounds/{swlat}/{swlng}/{nwlat}/{nwlng}', 'LocatorController@boundsBased');
    Route::get('/find/radius/filter/{min}/{max}/{lat}/{lng}/{rad}', 'LocatorController@radiusBasedFilter');
    Route::get('/find/bounds/filter/{min}/{max}/{swlat}/{swlng}/{nwlat}/{nwlng}', 'LocatorController@boundsBasedFilter');

});


