<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HostelPolicy extends Model
{
    //
    function hostel(){
    	return $this->belongsTo('App\Hostel');
    }
}
