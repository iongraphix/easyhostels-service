<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HostelRoom extends Model
{
    //
    public function hostel()
    {
        return $this->belongsTo('App\Hostel');
    }
    public function users_who_booked_room(){
    	return $this->belongsToMany('App\User', 'hostel_room_bookings', 'hostel_room_id', 'user_id');
    }
}
