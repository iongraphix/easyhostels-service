<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacilityType extends Model
{
    public function hostels(){
    	return $this->belongsToMany('App\Hostel', 'hostel_facilities', 'ftype_id','hostel_id');
    }
}
