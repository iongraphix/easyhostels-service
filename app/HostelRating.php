<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HostelRating extends Model
{
    // Intermediary
    public function hostel(){
    	return $this->belongsTo('App\Hostel');
    }
}
