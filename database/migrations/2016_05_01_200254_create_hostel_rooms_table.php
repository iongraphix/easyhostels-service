<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostelRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hostel_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hostel_id')->unsigned();
            $table->boolean('available');
            $table->text('room_no');
            $table->integer('capacity');
            $table->double('head_price');
            $table->timestamps();
        });

        Schema::table('hostel_rooms', function (Blueprint $table) {
              $table->foreign('hostel_id')->references('id')->on('hostels')->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hostel_rooms');
    }
}
