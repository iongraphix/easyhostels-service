<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hostels', function (Blueprint $table) {
            $table->increments('id');
            $table->text('display_name');
            $table->text('address');
            $table->text('short_description');
            $table->text('contact_phone');
            $table->text('contact_email');
            $table->decimal('lat', 10, 8);
            $table->decimal('lng', 11, 8);
            $table->float('display_price');
            $table->integer('user_id')->unsigned();
            $table->tinyInteger('available');
            $table->text('thumbnail');
            $table->text('notes');
            $table->timestamps();
        });

        Schema::table('hostels', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hostels');
    }
}
