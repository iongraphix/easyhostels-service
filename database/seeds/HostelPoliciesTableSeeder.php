<?php

use Illuminate\Database\Seeder;
use App\HostelPolicy;

class HostelPoliciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
        //
        $policies = array(
                ['hostel_id' => 10,'policy' => 'No Smoking' ],
                ['hostel_id' => 11,'policy' => 'No Smoking' ],
                ['hostel_id' => 12,'policy' => 'No Smoking' ]
        );
        
        foreach ($policies as $policy)
        {
            HostelPolicy::create($policy);
        }
    }
}
