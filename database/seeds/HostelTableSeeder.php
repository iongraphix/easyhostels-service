<?php

use Illuminate\Database\Seeder;
use App\Hostel;

class HostelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
        $hostels = array(
                ['id' => 10,'display_name' => 'White Hostel', 'short_description' => 'The Whitest Hostel in Oyibi', 'contact_phone' => '0244343223', 'contact_email' => 'white@hostel.org', 'address' => 'White Hostel, Oyibi', 'lat' => 5.80315, 'lng' => -0.12212,'display_price' => 130.00, 'user_id' => 2, 'available' => true, 'notes' => 'Its All a white experience','thumbnail' => '' ],
                ['id' => 11,'display_name' => 'Black Hostel', 'short_description' => 'The Blackest Hostel in Oyibi', 'contact_phone' => '0542323423', 'contact_email' => 'black@hostel.org','address' => 'Black Hostel, Oyibi', 'lat' => 5.79828, 'lng' => -0.11920,'display_price' => 140.00, 'user_id' => 2, 'available' => true, 'notes' => 'Its All a Black experience','thumbnail' => '' ],
                ['id' => 12,'display_name' => 'Red Hostel', 'short_description' => 'The Redest Hostel in Oyibi', 'contact_phone' => '0274343223', 'contact_email' => 'red@hostel.org', 'address' => 'Red Hostel, Oyibi','lat' => 5.800461, 'lng' => -0.127823,'display_price' => 150.00, 'user_id' => 2, 'available' => true, 'notes' => 'Its All a Red experience','thumbnail' => '' ],
                ['id' => 13,'display_name' => 'Nasgda Hall', 'short_description' => 'Valley View University Girls Hall. ', 'contact_phone' => '0274343233', 'contact_email' => 'nagda@vvu.edu.gh','address' => 'AF 595 Adenta, Mile 20', 'lat' => 5.796575, 'lng' => -0.127094,'display_price' => 160.00, 'user_id' => 2, 'available' => true, 'notes' => 'Its All a Pink experience', 'thumbnail' => '' ]
        );

        foreach ($hostels as $hostel)
        {
            Hostel::create($hostel);
        }
    }
}
