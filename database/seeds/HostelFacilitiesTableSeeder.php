<?php

use Illuminate\Database\Seeder;
use App\HostelFacility;
class HostelFacilitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hfs = array(
                ['hostel_id' => 10,'ftype_id' => 1],
                ['hostel_id' => 10,'ftype_id' => 2],
                ['hostel_id' => 10,'ftype_id' => 3],
                ['hostel_id' => 11,'ftype_id' => 1],
                ['hostel_id' => 11,'ftype_id' => 2],
                ['hostel_id' => 11,'ftype_id' => 3],
                ['hostel_id' => 12,'ftype_id' => 1],
                ['hostel_id' => 12,'ftype_id' => 2],
                ['hostel_id' => 12,'ftype_id' => 3],
        );
        foreach ($hfs as $hf)
        {
            HostelFacility::create($hf);
        }
    }
}
