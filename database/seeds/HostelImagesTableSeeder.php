<?php

use Illuminate\Database\Seeder;
use App\HostelImagery;
class HostelImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $images = array(
                ['hostel_id' => 10,'source' => '' ],
                ['hostel_id' => 11,'source' => '' ],
                ['hostel_id' => 12,'source' => '' ]
        );
        
        foreach ($images as $image)
        {
            HostelImagery::create($image);
        }
    }
}
