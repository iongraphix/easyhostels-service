<?php

use Illuminate\Database\Seeder;
use App\HostelRoom;

class HostelRoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         $hrooms = array(
                ['hostel_id' => 10,'available' => true, 'room_no' => 'Room #1', 'capacity' => 2, 'head_price' => 200.00 ],
                ['hostel_id' => 10,'available' => true, 'room_no' => 'Room #2', 'capacity' => 2, 'head_price' => 200.00 ],
                ['hostel_id' => 10,'available' => true, 'room_no' => 'Room #3', 'capacity' => 2, 'head_price' => 200.00 ],
                ['hostel_id' => 11,'available' => true, 'room_no' => 'Room #1', 'capacity' => 2, 'head_price' => 200.00 ],
                ['hostel_id' => 11,'available' => true, 'room_no' => 'Room #2', 'capacity' => 2, 'head_price' => 200.00 ],
                ['hostel_id' => 11,'available' => true, 'room_no' => 'Room #3', 'capacity' => 2, 'head_price' => 200.00 ],
                ['hostel_id' => 12,'available' => true, 'room_no' => 'Room #1', 'capacity' => 2, 'head_price' => 200.00 ],
                ['hostel_id' => 12,'available' => true, 'room_no' => 'Room #2', 'capacity' => 2, 'head_price' => 200.00 ],
                ['hostel_id' => 12,'available' => true, 'room_no' => 'Room #3', 'capacity' => 2, 'head_price' => 200.00 ]
        );
        foreach ($hrooms as $hroom)
        {
            HostelRoom::create($hroom);
        }
    }
}
