<?php

use Illuminate\Database\Seeder;
use App\HostelRating;
class HostelRatingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        //
        $ratings = array(
                ['hostel_id' => 10,'user_id' => 1, 'rating' => 5, 'review' => 'I love it here'],
                ['hostel_id' => 10,'user_id' => 4, 'rating' => 2, 'review' => 'I hate it here'],
                ['hostel_id' => 11,'user_id' => 1, 'rating' => 5, 'review' => 'I love it here'],
                ['hostel_id' => 11,'user_id' => 4, 'rating' => 2, 'review' => 'I hate it here'],
                ['hostel_id' => 12,'user_id' => 1, 'rating' => 5, 'review' => 'I love it here'],
                ['hostel_id' => 12,'user_id' => 4, 'rating' => 2, 'review' => 'I hate it here'],
        );
        foreach ($ratings as $rating)
        {
            HostelRating::create($rating);
        }
    }
}
