<?php

use Illuminate\Database\Seeder;
use App\FacilityType;

class FacilityTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        //
        $types = array(
                ['title' => 'Free-Wifi', 'description' => 'Free Access to Wi-Fi','md-icon' => 'md-wifi'],
                ['title' => 'Air-Conditioning', 'description' => 'Cool and Air-Conditioned','md-icon' => 'md-ac'],
                ['title' => 'Kitchen', 'description' => 'A Good Clean place to cook food','md-icon' => 'md-kitchen'],
                ['title' => 'Airport Transfer', 'description' => 'Pickup from the Airport and back','md-icon' => 'md-airport'],
                ['title' => 'Parking', 'description' => 'A Spacious Parking space for your vehicles','md-icon' => 'md-parking'],
                ['title' => 'Linen-Included', 'description' => 'Linen Included - Towels,Napkins','md-icon' => 'md-linen'],
                ['title' => 'Luggage Storage', 'description' => 'A Nice spacious place to store you luggage','md-icon' => 'md-luggage'],
                ['title' => 'Lockers', 'description' => 'Safe storage locations','md-icon' => 'md-lockers']
        );

        foreach ($types as $type)
        {
            FacilityType::create($type);
        }
    }
}
