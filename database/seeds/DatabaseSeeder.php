<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FacilityTypesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(HostelTableSeeder::class);
        $this->call(HostelFacilitiesTableSeeder::class);
        $this->call(HostelRoomsTableSeeder::class);
        $this->call(HostelPoliciesTableSeeder::class);
        $this->call(HostelImagesTableSeeder::class);
        $this->call(HostelRatingsTableSeeder::class);
    }
}
