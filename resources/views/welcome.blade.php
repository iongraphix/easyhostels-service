<!DOCTYPE html>
<html>
    <head>
        <title>Easy Hostels | REST API</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
                background: whitesmoke;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">REST API</div>
                <p style="font-family:'Lato';font-weight: bold;">By. Peace Okenwa</p>
                <p style="font-family:'Lato';font-weight: bold;">/api/v1/</p>
            </div>
        </div>
    </body>
</html>
